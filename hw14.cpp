﻿#include <iostream>
#include <string>
using namespace std;
/*
переменную типа std::string любым значением.
Вывести саму строковую переменную, вывести длину строки,
вывести первый и последний символы этой строки.
Для вывода использовать std::cout <<
*/

int main()
{
	
	cout << "Enter you name: " << endl;
	string YouName;
	getline(cin, YouName);

	cout << "Your name  " << YouName << " " << "consists of  " 
		 << YouName.length() << " letters" <<  "\n";
	cout << endl;
	
	cout << "the first letter in you name:  " << " " << YouName[0] << endl;
	cout << "the last letter in you  name:  " << " " << YouName[YouName.size() - 1]<<endl;
	
	system("pause");

}
